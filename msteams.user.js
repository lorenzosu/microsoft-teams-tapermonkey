// ==UserScript==
// @name     msteams
// @version  0.1
// @author Lorenzo Sutton
// @include  https://teams.microsoft.com/
// @include  https://teams.microsoft.com/*
// @require https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.5/push.min.js
// @grant GM_getValue
// @grant GM_setValue
// @grant GM_notification
// ==/UserScript==

var scriptTitle = "MSTEAMGM";
scriptConsole("Hello MSTEAMS... MS you suck :P");
var counter = 0;
var notificationResetTimes = 40; // after these times a notification is skipped reset
var originalTitle = "";

var notificationSent = 0;
var patt = /\(\d+\)/g;
var i = setInterval(doCheck, 15000);

function scriptConsole(myMessage) {
    console.log(scriptTitle + ":", myMessage);
}

function doReset() {
    counter = 0;
    notificationSent = 0;
    originalTitle = "";
    scriptConsole("Resetting notifications");
}

function doCheck() {
	var x = document.title;
	if (x != originalTitle) {
		originalTitle = x;
		var res = patt.test(x);
        counter += 1;
		if ((res == true) && (notificationSent == 0)) {
			scriptConsole(x);
			doNotification();
			scriptConsole("Notified");
			notificationSent = 1;
		}
        if (counter > notificationResetTimes) {
            doReset();
        }
	} else {
		notificationSent = 0;
	}
}

function doNotification() {
	Push.create("MS Teams", {
        title: "Teams",
		body: "Chats or activity detected",
		icon: "https://statics.teams.microsoft.com/hashedassets/favicon/prod/favicon-32x32-9c4d1a6e.png",
		onClick: function () {
            window.focus();
            scriptConsole(this);
            this.close();
		}
	});
}