Tampermonkey (https://tampermonkey.net/) script workaround for getting basic
activity (desktop) notifications on Linux from the Microsoft Teams website which
natively lacks this feature (see e.g. http://tomtalks.uk/2018/02/microsoft-teams-linux-whats-story/)